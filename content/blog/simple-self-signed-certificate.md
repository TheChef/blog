---
title: "Very simple self generated/signed TLS certificate"
date: 2020-04-06T00:02:46+02:00
draft: false
---

Need an extremely simple self signed certificate? Sometimes you just need a very simple TLS certificate for local communications or for a simple development environment. 

Use the following command to generate a simple certificate:

```
openssl req -new -newkey rsa:4096 -days 9999 -nodes -x509 -keyout server.key -out server.crt
```