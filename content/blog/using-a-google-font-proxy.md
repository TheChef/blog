---
title: "Using a Google font proxy"
date: 2019-09-01T00:12:07+02:00
draft: false
---

Google Web Fonts is a widely used method of easily including fonts into your website. It makes it very easy to select and use on of the fonts made available, through google it's CDN. This sound very nice of them to do, however with all things that Google does: if you are not paying, you are the product.

# Privacy concerns

Another product that is often used are things like Google Tag Manager in combination with Google Analytics. Especially Google Analytics is used by many websites as a tool to track their users. However, by enabling Google Analytics we are essentially also handing all this information to Google and Google can use this information to track users not just on this single website but across all other websites using Analytics. As of writing this article, [Builtwith](https://trends.builtwith.com/analytics/Google-Analytics) knows of at least 27 million websites using Google Analytics. 

Even more websites use Google Fonts, over [**38 million websites**](https://trends.builtwith.com/widgets/Google-Font-API) use Google Fonts. Google can use Google Fonts just like it uses Analytics, and can track all of the users across all of these websites. 

## Does Google track you using cookies?
Google Fonts does not set any cookie, this however does not rule out tracking. A technology called *Browser Finger Printing* can still very well be used to track unique browsers all over the internet. Browser finger printing basically means that your browser sends so much information on your machine to the websites you are visiting that it makes you uniquely identifiable. Information like screen resolution, user-agent, screen color depth, your timezone, a list of browser plugins ans the OS you are using. All this information combined paints a unique picture of you and your browser.  More information on browser finger printing can be found on the website of [EFF](https://panopticlick.eff.org/about).

This could very well be used to track you and your users.

## Browser caching
Requesting a font with Google Fonts return a header that tells the browser to cache the CSS needed for the font for 24 hours. The font itself is cached for 1 year. 

**fonts.googleapis.com (serves CSS)**:
```
cache-control: private, max-age=86400
```
**fonts.gstatic.com (serves font files)**:
```
cache-control: private, max-age=31536000
```

This means that for this font family this browser will not request the same CSS for 24 hours. If the browser sees a new request that has not been cached it will request this from Google. Google fonts allows for many font combinations and these combinations can very well be unique like:

* https://fonts.googleapis.com/css?family=Chilanka|Gayathri|Roboto&display=swap
* https://fonts.googleapis.com/css?family=Lora|Roboto+Slab&display=swap
* https://fonts.googleapis.com/css?family=Lora|Muli|Nunito&display=swap

And many many more combinations are possible between all the fonts. For each of these combination the browser will make a request to Google every 24 hours. It is hard to determine how often these requests would be done in the real world, but this would still be a lot of data being sent to Google.

# Using a Google Font proxy
By passing all the request through a reverse proxy we can essentially turn all the requests from the browser into anonymous requests. All the users pass the request to the proxy and the proxy passes the request to Google as if it came from itself. It will then pass the result of the request to the user.

# Using bruijns.org Font proxy
I built a Google Font proxy for usage with my own website. Feel free to use this for your own purpose.

Replace all the instances of the Google fonts address in your websites:
```
https://fonts.googleapis.com/
https://fonts.gstatic.com/
```
To:
```
https://googlefontproxy.bruijns.org/
```
And the reverse proxy will start to anonymise all the requests.

# Built your own Google font proxy using nginx
This is also very easy to do. Install nginx on your os of choice and create a new vhost. Refer to the following code snippet for the proxy:

```
server
{

    server_name            <your domain>;

    listen                 443 http2 ssl;
    ssl_certificate        /path/to/certificate.crt;
    ssl_certificate_key    /path/to/key.key;

    access_log              /var/log/nginx/<your domain>access.log main;
    error_log               /var/log/nginx/<your domain>.error.log error;


    location / {
            proxy_pass https://fonts.googleapis.com/;
            proxy_set_header Host fonts.googleapis.com;
            proxy_set_header Accept-Encoding "";

            subs_filter_types text/css;
            subs_filter //fonts.gstatic.com/ //<your domain>/fonts/;
    }

    location /fonts {
            proxy_pass https://fonts.gstatic.com/;
            proxy_set_header Host fonts.gstatic.com;
    }

}
          
``` 

Replace the above with your own domain and get a SSL certificate for it. It is crucial to run your font proxy on a TLS encrypted connection.