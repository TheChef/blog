---
title: "Gebruik CAA records (NL)"
date: 2018-04-26T00:12:07+02:00
draft: false
---

Op het internet is het lastig om te bepalen wie te vertrouwen is en wie niet. Daarom vragen we aan partijen die we expliciet wel vertrouwen om voor ons de identiteit van websites te controleren. Dit doen ze door een certificaat af te geven voor deze websites, een SSL certificaat.

De bedrijven die deze verificaties uitvoeren zijn zogenoemde CA’s of Certificate Authorities. Voorbeelden van deze CA’s zijn Comodo, Symantec of GlobalSign. De beheerder van een website vraagt voor zijn domein een SSL certificaat aan en de CA voert de controle uit om zeker te zijn dat de aanvrager ook daadwerkelijk de persoon of het bedrijf is dat deze claimt te zijn. Vervolgens geven ze het certificaat af dat vervolgens kan worden geïnstalleerd op de webserver. Het resultaat is dat de partijen die verbinden met deze website weten dat ze ook echt daadwerkelijk bij de juiste website zijn, en dus met een gerust hart hun inloggegevens kunnen gebruiken.

Helaas is het in het verleden niet altijd goed gegaan met de CA’s. Naast de bedrijven die hier boven zijn genoemd, zijn er nog meer CA’s en niet iedereen heeft zich in het verleden even netjes aan de spelregels gehouden. Er zijn CA’s geweest die het verificatieproces niet altijd even goed op orde hebben gehad, met als gevolg dat er certificaten werden uitgegeven aan bedrijven en personen voor domeinen die zij eigenlijk niet in hun beheer hadden. Dit stelde ze in staat om zich voor te doen als deze domeinen. Als dit naar buiten kwam, leidde dit nagenoeg altijd tot een groot schandaal. Het waren namelijk juist deze bedrijven waarin we het vertrouwen stelden om de aanvragers goed te controleren.

Een bekend voorbeeld van zo’n schandaal is DigiNotar, een bedrijf dat zich onder andere bezighield met alle certificaten voor de Nederlandse overheid. In juni 2011 werd er bij DigiNotar ingebroken en lukte het een hacker om certificaten voor zichzelf uit te geven zonder de juiste validatie te doorlopen. Zo werden er certificaten afgegeven voor bijvoorbeeld *.google.com.

# Wat doet een CAA record?

Een CAA record is een DNS record. Hierin kun je vast laten leggen welke CA’s geautoriseerd zijn om voor het domein waar je het record aan toevoegt een certificaat uit te geven. Op deze manier kan je bepalen dat bijvoorbeeld alleen Let’s Encrypt of Comodo SSL certificaten mogen afgeven voor het domein. Dit biedt je dus de mogelijk om meer controle uit te oefenen op wie deze uitgiftes mag doen. Daarnaast kan je in het CAA record opgeven of je wilt dat er contact wordt gezocht als er een certificaat wordt aangevraagd bij een CA die je niet hebt opgenomen in het CAA record. Je kan dan een e-mailadres specificeren dat hiervoor gebruikt moet worden.

# Hoe ziet een CAA record er uit?

Een voorbeeld van een CAA-record ziet eruit als hieronder:
```
0 issue "comodoca.com"
0 issue "letsencrypt.org"
0 iodef "mailto:certificates@bruijns.org"
```

Elk record hierboven bestaat uit drie onderdelen: flag, tag en value.

* flag: De flag is een bit-waarde. Dit is altijd 0 of 128. Dit is verplicht, maar voornamelijk aanwezig voor de toekomstbestendigheid.
* De tag geeft aan om wat voor type record het gaat. Dit kunnen op dit moment 3 waardes zijn:
  * issue – geeft aan welke CA er vertrouwd is.
  * issuewild – doet hetzelfde als issue, maar enkel voor wildcard certificaten.
  * iodef – dit is de manier waarop we specificeren op welke wijze een CA die niet geautoriseerd is contact opneemt (d.m.v Incident Object Description Exchange Format) als er toch een aanvraag namens het domein wordt gedaan. In de meeste gevallen is dit: mailto:emailadres maar dit kan ook een http call zijn.
* value: In dit veld nemen we de waarde van het veld op, in quotes.

In het bovenstaande voorbeeld mogen dus Comodo en Let’s Encrypt SSL certificaten uitgeven voor het bewuste domein. Als dat bij een andere CA gebeurt, zal deze eerst contact opnemen via certificates@bruijns.org

Je kunt dus aardig wat aangeven in een CAA record. Als je dit niet goed instelt, dan kan het resultaat zijn dat een CA geen certificaat voor je domein kan uitgeven. Heb je geen CAA record gespecificeerd? Dan zijn in principe alle CA’s gemachtigd om SSL certificaten uit te geven als je deze aanvraagt en vertrouw je erop dat al deze bedrijven goed de regels volgen.

# SSLlabs en score

SSL labs is een tool van Qualys en een industrie standaard om de veiligheid van verbindingen te meten. SSL Labs geeft een Amerikaanse stijl van een cijfer voor de veiligheid van de verbinding, waar een **f** heel slecht is en een **A+** fantastisch. Heeft een domein geen CAA record, wordt in de toekomst het cijfer gelimiteerd tot een **B**

Kijk voor meer informatie op: https://www.ssllabs.com/