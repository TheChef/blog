FROM alpine/git
COPY . /src
WORKDIR /src
RUN git submodule init; git submodule update

FROM klakegg/hugo:0.49.2-busybox
COPY --from=0 /src/ /src/
RUN date
RUN hugo -d /src/public; mv /src/public /dist

FROM nginx
RUN rm -R /usr/share/nginx/html
COPY --from=1 /dist /usr/share/nginx/html